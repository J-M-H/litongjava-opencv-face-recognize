package com.litong.modules.face.recognize.dataobj;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bill robot
 * @date 2020年9月6日_下午7:58:06 
 * @version 1.0 
 * @desc
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetectResult {
  //图片Id
  private Long id;
  private boolean containFace, containEye;
  // 人脸数量
  private int faceCount;
  // 眼睛数量,一般是1,因为模型默认仅检测1个
  private int eyeCount;
  // 包含矩形的图片地址
  private String[] rectImageUris;
  //使用时间,毫秒
  private long useMillisecond;

}
