### litongjava-opencv-face-recognize

#### 简介

基于opecn cv封装的人脸识别项目,对外提供http api调用接口

#### 架构

jfinal+jocab

#### 演示视频
https://www.bilibili.com/video/bv1k54y117bo

#### 导入eclipse

1.新建数据库 \src\main\resouces\doc

2.导入eclipse maven项目直接导入即可

3.登录界面, 默认账号和密码是 admin/admin

#### 接口

litongjava-opencv-face-recognize接口文档
1.信息查询
1.1.判断模型文件是否存在  
登录,登录成功之后携带cookie才可以调用其他接口,

1.2.判断模型文件是否存在
接口地址	http://192.168.0.10:11020/face-recognize/face/xmlPath  
接口简介	返回文件路径,判断文件是否存在  
接口方式	post/get  

请求参数  
无  

返回信息,请求成功返回信息如下  
```
{
    "data": {
        "frontalfaceAltXmlExists": true,
        "frontalfaceAltXml": "D:\\dev_workspace\\java\\litong-project\\litongjava-opencv-face-recognize\\target\\classes\\opencv-4.1.1\\sources\\data\\haarcascades\\haarcascade_frontalface_alt.xml",
        "eyeXml": "D:\\dev_workspace\\java\\litong-project\\litongjava-opencv-face-recognize\\target\\classes\\opencv-4.1.1\\sources\\data\\haarcascades\\haarcascade_eye.xml",
        "eyeXmlExists": true
    },
    "code": 0,
    "msg": "执行成功"
}
```
2.人脸接口  
2.1.人脸检测  
接口地址	http://192.168.0.10:11020/face-recognize/face/detect  
接口简介	上传图片,检测图片中是否包含人脸  
接口方式	post/get  

请求参数  
key	desc  
file	binary 上传图片  

返回参数  
有人脸信息  
```
{
  "data": {
    "useMillisecond": 1502,
    "eyeCount": 5,
    "containFace": true,
    "faceCount": 2,
    "rectImageUris": [
      "face/face_detect_photo_2020092910290119.jpg"
    ],
    "containEye": true
  },
  "code": 0,
  "msg": "执行成功"
}
```
无人脸信息
```
{
  "data": {
    "useMillisecond": 0,
    "eyeCount": 0,
    "containFace": false,
    "faceCount": 0,
    "rectImageUris": null,
    "containEye": false
  },
  "code": 0,
  "msg": "执行成功"
}
```
2.2.查看人脸检测结果  
接口地址	http://127.0.0.1:11020/face-recognize/image  
接口简介	查看系统中的图片  
接口方式	post/get  
请求参数  
filename	图片的uri    
假设人脸检测结果是face/face_detect_photo_2020092910290119.jpg  
请求下面的地址查看人脸
```
http://127.0.0.1:11020/face-recognize/image?filename=face/face_detect_photo_2020092910290119.jpg
```

2.3.上传人脸图片  
接口地址	http://127.0.0.1:11020/face-recognize/face/uploadImageFace  
接口简介	上传图片,检测图片中是否仅仅包含1张人脸  
接口方式	post/get  

请求参数  
key	desc  
file	binary 上传图片  

上传成功返回如下 id是图片的id,在添加人脸信息时会用到  
```
{
  "data": {
    "id": 3,
    "useMillisecond": 35,
    "faceCount": 1,
    "eyeCount": 3,
    "rectImageUris": [
      "face/face_detect_photo_20200929114520301.jpg"
    ],
    "containEye": true,
    "containFace": true
  },
  "code": 0,
  "msg": "执行成功"
}
```

2.4.人脸搜索  
接口地址	http://127.0.0.1:11020/face-recognize/face/search  
接口简介	查看系统中的图片  
接口方式	post/get  

请求参数  
key	desc  
file	binary 上传图片  

返回信息,返回搜索到的员工信息  



